package com.sjain.myacclerationspeed;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by sjain on 15/03/18.
 */

public class App extends Application implements Application.ActivityLifecycleCallbacks {

    public static FirebaseDatabase mFirebaseDatabase;

    public static String LOG_TAG = "MYACCELROMETER";

    @Override
    public void onCreate() {
        super.onCreate();
        String token = FirebaseInstanceId.getInstance().getToken();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
//        Log.d("Firebase TOken", token);
    }

    public static FirebaseDatabase getFirebaseDatabase() {
        if (mFirebaseDatabase != null) {
            return mFirebaseDatabase;
        } else {
            FirebaseDatabase mFirebaseDatabase = FirebaseDatabase.getInstance();
            return mFirebaseDatabase;
        }
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}
