package com.sjain.myacclerationspeed.interfaces;

/**
 * Created by sjain on 14/03/18.
 */

public interface AccelerometerListener {

    public void onAcceleration(float x, float y, float z, float accel);

}
