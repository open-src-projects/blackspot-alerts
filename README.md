

# Making Blackspots safer
India is an amazing country, best-explored via roads. However, it does come with its own set of challenges. The roads witness close to 500K accidents every year and a major contributor to that number, are road blackspots spread throughout the network of roads. Over 800 blackspots make, the otherwise improving highways, unsafe and deter drivers from exploring.
While the government is trying to address this massive problem by investing huge capital in infrastructure to reduce accidents at blind spots, we have built a simple, yet seamless, tech solution that could potentially minimize these blackspot related accidents drastically.
We are using an android app, which will eventually also be made available via an SDK for wider reach via fleet apps, to track the location of a driver, measure his/her speed and alert via audio and visual cues if he/she is driving at speeds higher than suitable while approaching one of the 800+blindspots.
Taking another step towards making the roads safer and improving driving behaviour of the masses, we are gamifying the whole process of driving. Using data from smartphone sensors like accelerometer and GPS among others, we are determining the driving style of drivers and recording events like aggressive braking, rapid acceleration and speeding. This allows us to tap into the emulous nature of humans and thus getting them to follow the rules of the road unknowingly.

* 500k Total road accidents on Indian Highways in 2016-17
* 726 Identified dangerous blackspots across India
* 220 Spots identified as most accident prone black spots mostly on the NH2

## Numbers are alarming!
#### What's the reason?

* Because the drivers are unable to estimate how sharp the bend is, causing drivers to take very risky turns on bends.
* Government has earmarked Rs 11,000 crore to address this problem
* Signs / Markings is the Short Term Solution that the Government has come up with

## Our Solution
* Smart Blackspot Alerts displayed on the drivers’ mobile phones if the speeds are above the threshold for the upcoming bend
* These alerts will work even when the drivers’ phone is offline
* Research suggests that humans instinctively react more to sound than visual.
* Hence, these Alerts will create a loud sound on the drivers’ mobiles and it will keep playing until the driving speed is below the threshold

## How?
#### By leveraging technology
* Algorithm to detect whether the user is driving or not
* Firebase
* GPS Tracking
* Accelerometer Sensor
* Algorithm to detect harsh acceleration / rash driving
* Detecting Phone Usage while driving

## Humans love when they are RECOGNIZED / APPRECIATED
#### The GAMIFICATION feature:
- Will help us Identify & Reward Top drivers who are scored on parameters such as:
    - Harsh Acceleration
    - Over speeding
    - Phone Usage
- Will encourage others to drive safe thereby, creating an ecosystem of safe driving

## Potential impact
* Reduced black spot accidents using timely alerts
* Better road safety education with deep driving data
* Tapping into the competitive nature of people by Gamifying the driving behaviour and motivate them to drive better

# ScreenShots

![Screen 1](https://www.dropbox.com/s/a5ttd87cw8n3dvq/bs_1.png?dl=1)

![Screen 2](https://www.dropbox.com/s/1nxknbmmdg177f1/bs_2.png?dl=1)

![Screen 3](https://www.dropbox.com/s/p95pyvhi3vk39on/bs_3.png?dl=1)